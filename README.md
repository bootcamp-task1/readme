# ACE Bootcamp project 2024

Your task is to collectively deploy an application, composed of 3 separate services.

The components to deploy are the following:

- services
    - users backend
        - requires aPostgreSQL database
    - images backend
        - requires an S3 bucket
    - frontend application
        - connects to the 2 backend services

The project can be done either solo, or in a group of 2-3 people, where each person gets one service.

The frontend application connects to both backend services, but the functionality does not overlap, so it’s verifiable separately.

### Tasks

- Fork the service repository
- Create a Dockerfile
- Create a GitLab CI/CD pipeline to build and push the Docker image
- Deploy the dependency to Kubernetes
- Deploy the application to Kubernetes
- Update the GitLab CI/CD pipeline to update the application when code changes

### Requirements

- Work only inside your namespace - personal or team.
- Use the provided Ingress controller
- Do not provision a Service of type LoadBalancer
- Use the shared `*.bootcamp.opslifting.cz`  DNS
- The application should be accessible from the public internet
- (Optional) The application should be available via HTTPS with a valid certificate
- Use GitLab Agent to access the Kubernetes cluster from GitLab
